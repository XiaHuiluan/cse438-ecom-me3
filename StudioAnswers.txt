Names of student(s):

XML Questions:

What new files are there?

In the activity_product_detail.xml file, the ”ADD TO CART” Button has a background of@drawable/roundedbtn.  Go to this file in the drawables folder and analyze the XML to ensure thatyou understand what’s going on.

Kotlin Questions:

What is the name of our database?

How many tables does our SQLite database hold?  How do you know?

When our database upgrades to a new version, what happens?  Do you think this is how a databaseupdate should be handled in the real world?  Why or why not?